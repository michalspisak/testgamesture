﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject canvasPopup;
    public GameObject Text3sec;

    // Start is called before the first frame update
    void Start()
    {
        canvasPopup.SetActive(false);
        Text3sec.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ExitPopup()
    {
        canvasPopup.SetActive(false);
    }


    public void PlayCanvas()
    {
        canvasPopup.SetActive(true);
    }

    public void PlayText3sec()
    {
        Text3sec.SetActive(true);
        StartCoroutine(popupWait());
    }

    IEnumerator popupWait()
    {
        yield return new WaitForSeconds(3.0f);
        Text3sec.SetActive(false);

    }
}